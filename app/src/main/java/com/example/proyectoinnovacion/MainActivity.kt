package com.example.proyectoinnovacion

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener {

    //invocacion de fragmantos
    lateinit var traductorBlankFragment: TraductorBlankFragment
    lateinit var categoriasBlankFragment: CategoriasBlankFragment
    lateinit var diccionarioBlankFragment: DiccionarioBlankFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById(R.id.main_toolbar) as Toolbar?
        val toolbarTitle = findViewById(R.id.titleText) as TextView?
        val toolbarCounter = findViewById(R.id.titleCounter) as TextView?

        toolbar?.setTitle("")
        toolbarTitle?.setText("SIGN LANGUAGE")
        toolbarCounter?.setText("")

        setSupportActionBar(toolbar)

        val drawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            (R.string.close),
            (R.string.open)
           ){

        }

        drawerToggle.isDrawerIndicatorEnabled = true
        drawer.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        navigationView.setNavigationItemSelectedListener (this)

        traductorBlankFragment = TraductorBlankFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, traductorBlankFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId){
            R.id.Categorias -> {
                categoriasBlankFragment = CategoriasBlankFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, categoriasBlankFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.Compartir -> {
                diccionarioBlankFragment = DiccionarioBlankFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, diccionarioBlankFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.Traductor -> {
                traductorBlankFragment = TraductorBlankFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, traductorBlankFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
        }

        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
                drawer.closeDrawer(GravityCompat.START)
            }
            else {
            super.onBackPressed()
        }
    }
}
